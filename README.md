
# Corndog + Ketchup

A simple parser and markup tool for source code documentation generation.

## Set up development environment

Requirements:
* Python 3.9+
* Poetry 1.7+

```
poetry install
```

## Generate documentation

```
poetry run ./docs.sh
```

## Run tests

```
poetry run python -m unittest
```
