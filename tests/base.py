import os
import logging
import shutil
import unittest

TEST_ROOT = os.path.dirname(os.path.abspath(__file__))
OUTPUT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')

def path(*args):
	return os.path.join(TEST_ROOT, *args)

RETAIN_OUTPUTS = False

class CandKBaseCase(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		logging.getLogger().setLevel(logging.CRITICAL)
		os.makedirs(OUTPUT_DIR, exist_ok=True)

	@classmethod
	def tearDownClass(cls):
		if not RETAIN_OUTPUTS:
			try:
				os.remove(path('Corndog_out.txt'))
			except:
				pass
			try:
				shutil.rmtree(OUTPUT_DIR)
			except:
				pass


	def assertFileContents(self, filepath, expected):
		self.assertTrue(os.path.exists(filepath), 'Cannot find {}'.format(filepath))
		result = ''
		with open(filepath, 'r') as outfile:
			result = outfile.read()

		self.assertEqual(result, expected, '{} does not match expected'.format(filepath))
