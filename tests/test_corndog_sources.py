import contextlib
import io, os
import shutil
import unittest
from .base import CandKBaseCase, path, TEST_ROOT, OUTPUT_DIR

from candk.corndog import Corndog, STREAM

def generic_expected():
	return '''
 with corndog parsing part of it.


This is the class docstring. 


@method
def main(self):

This is a multi-line docstring.
Whoops the indentation is off.
That shouldn't matter.


@h1 There's stuff here.
And then the next line.

This adds a newline in between.



'''

def filetype(filetype):
	return path('data', 'filetypes', 'source'+filetype)


class TestCorndogSources(CandKBaseCase):

	def setUp(self):
		self.sut = Corndog(
			start_point = path('data', 'filetypes'),
			output = STREAM)

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - one per source file type
	#--------------------------------------------------

	def test_python(self):
		self.sut.start_point = filetype('.py')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_java(self):
		self.sut.start_point = filetype('.java')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_sql(self):
		self.sut.start_point = filetype('.sql')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_xml(self):
		self.sut.start_point = filetype('.xml')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_html(self):
		self.sut.start_point = filetype('.html')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_php(self):
		self.sut.start_point = filetype('.php')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_ftl(self):
		self.sut.start_point = filetype('.ftl')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_css(self):
		self.sut.start_point = filetype('.css')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_c(self):
		self.sut.start_point = filetype('.c')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_cpp(self):
		self.sut.start_point = filetype('.cpp')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_sh(self):
		self.sut.start_point = filetype('.sh')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_js(self):
		self.sut.start_point = filetype('.js')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_ts(self):
		self.sut.start_point = filetype('.ts')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_properties(self):
		self.sut.start_point = filetype('.properties')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_bat(self):
		self.sut.start_point = filetype('.bat')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_txt(self):
		self.sut.start_point = filetype('.txt')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_kp(self):
		self.sut.start_point = filetype('.kp')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_tas(self):
		self.sut.start_point = filetype('.tas')
		expected = generic_expected() + '\n'
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_vue(self):
		self.sut.start_point = filetype('.vue')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_toml(self):
		self.sut.start_point = filetype('.toml')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_yaml(self):
		self.sut.start_point = filetype('.yaml')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_yml(self):
		self.sut.start_point = filetype('.yml')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_conf(self):
		self.sut.start_point = filetype('.conf')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_ini(self):
		self.sut.start_point = filetype('.ini')
		expected = generic_expected()
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)