from .base import CandKBaseCase
from candk.ketchup.spread import Spread, InlineSpread, RenderTypes

class TestKetchupSpread(CandKBaseCase):

	def setUp(self):
		self.sut = Spread()

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - is_empty()
	#--------------------------------------------------

	def test_is_empty_1(self):
		'''self.text = '' '''
		self.sut.text = ''
		result = self.sut.is_empty()
		
		self.assertTrue(result)

	def test_is_empty_2(self):
		'''self.text = None '''
		self.sut.text = None
		result = self.sut.is_empty()
		
		self.assertTrue(result)

	def test_is_empty_2(self):
		'''self.text = 'Hello world!' '''
		self.sut.text = 'Hellow world!'
		result = self.sut.is_empty()
		
		self.assertFalse(result)

	#--------------------------------------------------
	# TEST CASES - make_render_safe()
	#--------------------------------------------------

	def test_make_render_safe_1(self):
		'''render type is HTML with brackets (<>) '''
		text = 'This is + content with <div> tags [in] it! </div>'
		render_type = RenderTypes.HTML
		expected = 'This is + content with &lt;div&gt; tags [in] it! &lt;/div&gt;'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_2(self):
		'''render type is HTML with no brackets '''
		text = 'This is + content with no div [tags] in it!'
		render_type = RenderTypes.HTML
		expected = 'This is + content with no div [tags] in it!'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_3(self):
		'''render type is Markdown with brackets ([]) '''
		text = 'This is <content> with [brackets] in it! [oh?]'
		render_type = RenderTypes.MARKDOWN
		expected = 'This is <content> with \[brackets] in it! \[oh?]'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_4(self):
		'''render type is Markdown with no brackets ([]) '''
		text = 'This is <content> with no brackets in it!'
		render_type = RenderTypes.MARKDOWN
		expected = 'This is <content> with no brackets in it!'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_5(self):
		'''render type is Markdown with plus sign (+) '''
		text = 'This + is <content> with some plus signs ++ whoo!'
		render_type = RenderTypes.MARKDOWN
		expected = 'This \+ is <content> with some plus signs \+\+ whoo!'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_6(self):
		'''render type is Markdown with no plus sign '''
		text = 'This is <content> with no plus signs'
		render_type = RenderTypes.MARKDOWN
		expected = 'This is <content> with no plus signs'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_7(self):
		'''render type is Markdown with brackets and plus sign '''
		text = 'This is <content> with [brackets] + plus signs!+ [oh?]'
		render_type = RenderTypes.MARKDOWN
		expected = 'This is <content> with \[brackets] \+ plus signs!\+ \[oh?]'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	def test_make_render_safe_8(self):
		'''render type is invalid type '''
		text = 'This is <content> with [brackets] + plus signs!+ [oh?]'
		render_type = None
		expected = 'This is <content> with [brackets] + plus signs!+ [oh?]'
		result = self.sut.make_render_safe(text, render_type)

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - render()
	#--------------------------------------------------

	def test_render_1(self):
		'''render type is HTML '''
		self.sut.text = 'This is <content> with [brackets] + plus signs!+ [oh?]'
		index = 120
		render_type = RenderTypes.HTML
		expected = '<div id="120" class="k-paragraph">This is &lt;content&gt; with [brackets] + plus signs!+ [oh?]</div>'
		result = self.sut.render(index, render_type)

		self.assertEqual(result, expected)

	def test_render_2(self):
		'''render type is Markdown '''
		self.sut.text = 'This is <content> with [brackets] + plus signs!+ [oh?]'
		index = 121
		render_type = RenderTypes.MARKDOWN
		expected = '\nThis is <content> with \[brackets] \+ plus signs!\+ \[oh?]\n'
		result = self.sut.render(index, render_type)

		self.assertEqual(result, expected)




class TestKetchupInlineSpread(CandKBaseCase):

	def setUp(self):
		self.sut = InlineSpread()

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - add_text()
	#--------------------------------------------------

	def test_add_text_1(self):
		'''preserve_linebreaks = False '''
		self.sut.add_text('This is a line!')
		self.sut.add_text('And another one.')
		self.sut.add_text('')
		self.sut.add_text('')
		self.sut.add_text('One more :)')
		expected = ' This is a line! And another one.   One more :)'
		result = self.sut.text

		self.assertEqual(result, expected)

	def test_add_text_2(self):
		'''preserve_linebreaks = True '''
		self.sut.add_text('This is a line!', preserve_linebreaks = True)
		self.sut.add_text('And another one.', preserve_linebreaks = True)
		self.sut.add_text('', preserve_linebreaks = True)
		self.sut.add_text('', preserve_linebreaks = True)
		self.sut.add_text('One more :)', preserve_linebreaks = True)
		expected = '@This is a line!@And another one.@@@One more :)'
		result = self.sut.text

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - determine_action()
	#--------------------------------------------------

	def test_determine_action_01(self):
		'''self.tag = codeline '''
		self.sut.tag = 'codeline'
		text = '/-dir/to the/file-/'
		i = 2
		char = 'd'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_codeline_inside')

	def test_determine_action_02(self):
		'''char = / and i = 0 '''
		self.sut.tag = None
		text = '/dir/to the/file'
		i = 0
		char = '/'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_codeline_open')

	def test_determine_action_03(self):
		'''char = / and i = 1 and text = /dir/to the/file
				and self.tag not code tag '''
		self.sut.tag = None
		text = ' /dir/to the/file'
		i = 1
		char = '/'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_codeline_open')

	def test_determine_action_04(self):
		'''char = ' ' and self.tag = code '''
		self.sut.tag = 'code'
		text = '/dir/to the/file'
		i = 7
		char = ' '
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_code_close')

	def test_determine_action_05(self):
		'''char = ! and i = 0  '''
		self.sut.tag = None
		text = '!dir/to the/file'
		i = 0
		char = '!'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_link_open')

	def test_determine_action_06(self):
		'''char = ! and i = 1 and text = !dir/to/file
				and self.tag not link tag '''
		self.sut.tag = None
		text = ' !dir/to the/file'
		i = 1
		char = '!'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_link_open')

	def test_determine_action_07(self):
		'''char = ' ' and self.tag = link '''
		self.sut.tag = 'link'
		text = '!dir/to the/file'
		i = 7
		char = ' '
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_link_close')

	def test_determine_action_08(self):
		'''char = * and self.tag is None '''
		self.sut.tag = None
		text = 'This is a *line.*'
		i = 10
		char = '*'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_bold_tag')

	def test_determine_action_09(self):
		'''char = * and self.tag is not None '''
		self.sut.tag = 'link'
		text = '!his is a *line.*'
		i = 10
		char = '*'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_plain_char')

	def test_determine_action_10(self):
		'''char = _ and self.tag is None '''
		self.sut.tag = None
		text = 'This is a _line._'
		i = 10
		char = '_'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_italic_tag')

	def test_determine_action_11(self):
		'''char = _ and self.tag is not None '''
		self.sut.tag = 'link'
		text = '!his is a _line._'
		i = 10
		char = '_'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_plain_char')

	def test_determine_action_12(self):
		'''char = @ and self.tag is None '''
		self.sut.tag = None
		text = 'This is a @line.'
		i = 10
		char = '@'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_linebreak')

	def test_determine_action_13(self):
		'''char = @ and self.tag is not None '''
		self.sut.tag = 'link'
		text = '!his is a @line.'
		i = 10
		char = '@'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_plain_char')

	def test_determine_action_14(self):
		'''char = ! and text = /hello! '''
		self.sut.tag = 'code'
		text = '/hello!'
		i = 6
		char = '!'
		result = self.sut.determine_action(i, char, text)

		self.assertEqual(result, '_plain_char')

	#--------------------------------------------------
	# TEST CASES - render_inline()
	#--------------------------------------------------

	def test_render_inline_1(self):
		'''normal line '''
		text = 'Hello World!'
		expected = 'Hello World!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_2(self):
		'''with escape character (\\) '''
		text = 'Hello \\*World!*'
		expected = 'Hello *World!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_3(self):
		'''text with inline link '''
		text = '!http://website.com/page?param=string,!*|'
		expected = '<a class="k-link" target="_blank" href="http://website.com/page?param=string,!*|">http://website.com/page?param=string,!*|</a> '
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_4(self):
		'''text with code '''
		text = 'Hello /World! wow!'
		expected = 'Hello <span class="k-code">World!</span> wow!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_5(self):
		'''text with codeline '''
		text = 'Hello /-to the-/ World!'
		expected = 'Hello <span class="k-code">to the</span> World!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_6(self):
		'''text with bold '''
		text = 'Hello *Wor*ld!'
		expected = 'Hello <strong>Wor</strong>ld!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_7(self):
		'''text with bold and italics '''
		text = 'He_llo *Wor*_ld!'
		expected = 'He<em>llo <strong>Wor</strong></em>ld!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)

	def test_render_inline_8(self):
		'''text with italics '''
		text = 'Hel_lo Wor_ld!'
		expected = 'Hel<em>lo Wor</em>ld!'
		result = self.sut.render_inline(text)

		self.assertEqual(result, expected)