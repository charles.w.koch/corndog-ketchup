import os
from datetime import datetime

from .base import CandKBaseCase, path, TEST_ROOT, OUTPUT_DIR
from candk.ketchup.spread import RenderTypes
from candk.ketchup.tags import get_spread
from candk.ketchup.base import Ketchup, VERSION

class TestKetchupBase(CandKBaseCase):

	def setUp(self):
		self.sut = Ketchup('temp')

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - translate_filename()
	#--------------------------------------------------

	def test_translate_filename_1(self):
		"""Unix filepath """
		filepath = 'dir/to/the/file.txt'
		result = self.sut.translate_filename(filepath, '/')

		self.assertEqual(result, 'dir.to.the.file')

	def test_translate_filename_2(self):
		"""Windows filepath """
		filepath = 'dir\\to\\the\\file.txt'
		result = self.sut.translate_filename(filepath, '\\')

		self.assertEqual(result, 'dir.to.the.file')

	#--------------------------------------------------
	# TEST CASES - initialize_data()
	#--------------------------------------------------

	def test_initialize_data_1(self):
		"""page_name = None """
		os.chdir(TEST_ROOT)
		self.sut.filepath = 'data/file1.txt'
		self.sut.page_name = None
		self.sut.output_name = 'candk_test'
		self.sut.output_dir = 'data/filetypes'
		self.sut.initialize_data()

		self.assertEqual(self.sut.page_name, 'data.file1')
		self.assertEqual(self.sut.output_name, 'candk_test')
		self.assertEqual(self.sut.output_dir, path('data', 'filetypes'))

	def test_initialize_data_2(self):
		"""output_name = None """
		os.chdir(TEST_ROOT)
		self.sut.filepath = 'data/file1.txt'
		self.sut.page_name = 'C and K test'
		self.sut.output_name = None
		self.sut.output_dir = 'data/filetypes'
		self.sut.initialize_data()

		self.assertEqual(self.sut.page_name, 'C and K test')
		self.assertEqual(self.sut.output_name, 'data.file1')
		self.assertEqual(self.sut.output_dir, path('data', 'filetypes'))

	def test_initialize_data_3(self):
		"""output_dir = None """
		os.chdir(TEST_ROOT)
		self.sut.filepath = 'data/file1.txt'
		self.sut.page_name = 'C and K test'
		self.sut.output_name = 'candk_test'
		self.sut.output_dir = None
		self.sut.initialize_data()

		self.assertEqual(self.sut.page_name, 'C and K test')
		self.assertEqual(self.sut.output_name, 'candk_test')
		self.assertEqual(self.sut.output_dir, path())

	def test_initialize_data_4(self):
		"""all defaults (None) """
		os.chdir(TEST_ROOT)
		self.sut.filepath = 'data/file1.txt'
		self.sut.page_name = None
		self.sut.output_name = None
		self.sut.output_dir = None
		self.sut.initialize_data()

		self.assertEqual(self.sut.page_name, 'data.file1')
		self.assertEqual(self.sut.output_name, 'data.file1')
		self.assertEqual(self.sut.output_dir, path())

	#--------------------------------------------------
	# TEST CASES - minify_css()
	#--------------------------------------------------

	def test_minify_css(self):
		expected = '/* this is comment */.classname {things-in-class: 23px;another-attr: 1px solid red;}.object > p {thing-in-here: value; /* comment */}#idforme {styling: none;}'
		filepath = path('data', 'not-mini.css')
		result = self.sut.minify_css(filepath)

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - render_nav_entry()
	#--------------------------------------------------

	def test_render_nav_entry_1(self):
		"""@h1 spread """
		self.sut.collapse_h1 = False
		id = 101
		self.sut.section = None
		spread = get_spread('@h1')
		spread.add_text('Nav Item 1')
		expected = '<a class="k-a" href="#101"><div class="k-nav k-nav-header k-nh1 collapsible"> Nav Item 1</div></a>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_2(self):
		"""@h1 spread with collapsible nav """
		self.sut.collapse_h1 = True
		id = 102
		self.sut.section = None
		spread = get_spread('@h1')
		spread.add_text('Nav Item 2')
		expected = '<details><summary class="k-nav k-nav-header k-nh1 collapsible"><a class="k-a" href="#102"> Nav Item 2</a></summary>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_3(self):
		"""@h1 spread with previous section and collapsible nav """
		self.sut.collapse_h1 = True
		id = 103
		self.sut.section = 112
		spread = get_spread('@h1')
		spread.add_text('Nav Item 3')
		expected = '</details>\n<details><summary class="k-nav k-nav-header k-nh1 collapsible"><a class="k-a" href="#103"> Nav Item 3</a></summary>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_4(self):
		"""@h2 spread """
		self.sut.collapse_h1 = True
		id = 104
		self.sut.section = None
		spread = get_spread('@h2')
		spread.add_text('Nav Item 4')
		expected = '<a class="k-a" href="#104"><div class="k-nav k-nav-header k-nh2"> Nav Item 4</div></a>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_5(self):
		"""@h3 spread """
		self.sut.collapse_h1 = False
		id = 105
		self.sut.section = None
		spread = get_spread('@h3')
		spread.add_text('Nav Item 5')
		expected = ''
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_6(self):
		"""@class spread """
		self.sut.collapse_h1 = False
		id = 106
		self.sut.section = None
		spread = get_spread('@class')
		spread.add_text('Nav Item 6')
		expected = '<a class="k-a" href="#106"><div class="k-nav k-nav-class"> Nav Item 6</div></a>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	def test_render_nav_entry_6(self):
		"""@method spread """
		self.sut.collapse_h1 = False
		id = 107
		self.sut.section = None
		spread = get_spread('@method')
		spread.add_text('Nav Item 7')
		expected = '<a class="k-a" href="#107"><div class="k-nav k-nav-method"> Nav Item 7</div></a>\n'
		result = self.sut.render_nav_entry(id, spread)

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - render_nav()
	#--------------------------------------------------

	TEST_DOC = [
		get_spread('@m2'),
		get_spread('@table'),
		get_spread('@h2'),
		get_spread('@h1'),
		get_spread('@byline'),
		get_spread(),
		get_spread(),
		get_spread('@h2'),
		get_spread('@hr'),
		get_spread(),
		get_spread('@h2'),
		get_spread(''),
		get_spread('@m1'),
		get_spread(''),
		get_spread('@h1'),
		get_spread(''),
		get_spread('@h1'),
		get_spread('@h2'),
		get_spread(''),
	]

	def test_render_nav_1(self):
		"""Regular nav (no collapses) """
		self.sut.collapse_h1 = False
		self.sut.document = self.TEST_DOC
		expected = '''<div class="k-navbar">
<a class="k-a" href="#2"><div class="k-nav k-nav-header k-nh2"></div></a>
<a class="k-a" href="#3"><div class="k-nav k-nav-header k-nh1 collapsible"></div></a>
<a class="k-a" href="#7"><div class="k-nav k-nav-header k-nh2"></div></a>
<a class="k-a" href="#10"><div class="k-nav k-nav-header k-nh2"></div></a>
<a class="k-a" href="#14"><div class="k-nav k-nav-header k-nh1 collapsible"></div></a>
<a class="k-a" href="#16"><div class="k-nav k-nav-header k-nh1 collapsible"></div></a>
<a class="k-a" href="#17"><div class="k-nav k-nav-header k-nh2"></div></a>
</div>
'''
		result = self.sut.render_nav()

		self.assertEqual(result, expected)

	def test_render_nav_2(self):
		"""Regular nav with collapsible h1 """
		self.sut.collapse_h1 = True
		self.sut.document = self.TEST_DOC
		expected = '''<div class="k-navbar">
<a class="k-a" href="#2"><div class="k-nav k-nav-header k-nh2"></div></a>
<details><summary class="k-nav k-nav-header k-nh1 collapsible"><a class="k-a" href="#3"></a></summary>
<a class="k-a" href="#7"><div class="k-nav k-nav-header k-nh2"></div></a>
<a class="k-a" href="#10"><div class="k-nav k-nav-header k-nh2"></div></a>
</details>
<details><summary class="k-nav k-nav-header k-nh1 collapsible"><a class="k-a" href="#14"></a></summary>
</details>
<details><summary class="k-nav k-nav-header k-nh1 collapsible"><a class="k-a" href="#16"></a></summary>
<a class="k-a" href="#17"><div class="k-nav k-nav-header k-nh2"></div></a>
</details>
</div>
'''
		result = self.sut.render_nav()

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - render_html_head() & render_html_footer()
	#--------------------------------------------------

	def test_render_html_head(self):
		self.sut.css_file = path('data', 'not-mini.css')
		expected = '''<html><head>
<title>file1</title>
<style>/* this is comment */.classname {things-in-class: 23px;another-attr: 1px solid red;}.object > p {thing-in-here: value; /* comment */}#idforme {styling: none;}</style>
</head><body>
<a class="k-a" href=""><div class="k-page-title">path.to.file1</div></a>
<div class="k-page">
<div class="k-navbar">
</div>

<div class="k-content">
'''
		result = self.sut.render_html_head('path.to.file1')

		self.assertEqual(result, expected)

	def test_render_html_footer(self):
		time_now = datetime.now()
		time_str = time_now.strftime('%I:%M%p  %d %b %Y')
		expected = '''<div class="k-spacer">Generated {}</br>
Documentation generated by Ketchup v{}</br>
Developed by Charles Koch - 2024</div>
</div></div></body></html>'''.format(time_str, VERSION)
		result = self.sut.render_html_footer(right_now=time_now)

		self.assertEqual(result, expected)