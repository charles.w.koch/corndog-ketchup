import os

from .base import CandKBaseCase, path, TEST_ROOT, OUTPUT_DIR
from candk.ketchup.spread import RenderTypes
from candk.ketchup.tags import get_spread
from candk.ketchup.base import Document


class TestDocument(CandKBaseCase):

	def setUp(self):
		self.sut = Document('temp')

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - check_for_output_controls()
	#--------------------------------------------------

	def test_check_for_output_controls_1(self):
		"""prev_line = @page """
		self.sut.prev_line = '@page'
		self.sut.check_for_output_controls('thevalue')
		
		self.assertEqual(self.sut.page_name, 'thevalue')
		self.assertEqual(self.sut.output_name, None)
		self.assertEqual(self.sut.output_dir, None)

	def test_check_for_output_controls_2(self):
		"""prev_line = @file """
		self.sut.prev_line = '@file'
		self.sut.check_for_output_controls('thevalue')
		
		self.assertEqual(self.sut.page_name, None)
		self.assertEqual(self.sut.output_name, 'thevalue')
		self.assertEqual(self.sut.output_dir, None)

	def test_check_for_output_controls_3(self):
		"""prev_line = @outfile """
		self.sut.prev_line = '@outdir'
		self.sut.check_for_output_controls('thevalue')
		
		self.assertEqual(self.sut.page_name, None)
		self.assertEqual(self.sut.output_name, None)
		self.assertEqual(self.sut.output_dir, 'thevalue')

	def test_check_for_output_controls_4(self):
		"""prev_line is not @page, @file, or @outdir """
		self.sut.prev_line = '@h3'
		self.sut.check_for_output_controls('thevalue')
		
		self.assertEqual(self.sut.page_name, None)
		self.assertEqual(self.sut.output_name, None)
		self.assertEqual(self.sut.output_dir, None)

	#--------------------------------------------------
	# TEST CASES - no_spread_or_not_in()
	#--------------------------------------------------

	def test_no_spread_or_not_in_1(self):
		"""self.spread = None """
		self.sut.spread = None
		result = self.sut.no_spread_or_not_in(['@h1', '@m1'])

		self.assertTrue(result)

	def test_no_spread_or_not_in_2(self):
		"""self.spread is not in taglist """
		self.sut.spread = get_spread('@note')
		result = self.sut.no_spread_or_not_in(['@table', '@h1', '@m1'])

		self.assertTrue(result)

	def test_no_spread_or_not_in_3(self):
		"""self.spread is in taglist """
		self.sut.spread = get_spread('@table')
		result = self.sut.no_spread_or_not_in(['@table', '@h1', '@m1'])

		self.assertFalse(result)

	#--------------------------------------------------
	# TEST CASES - determine_action()
	#--------------------------------------------------

	def test_determine_action_01(self):
		"""self.spread = None and a valid ketchup tag"""
		self.sut.spread = None
		line = '@attributes'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'styled_tag')

	def test_determine_action_02(self):
		"""self.spread is not a non-styled tag"""
		self.sut.spread = get_spread('@table')
		line = '@note'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'styled_tag')

	def test_determine_action_03(self):
		"""self.spread is a non-styled tag"""
		self.sut.spread = get_spread('@class')
		line = '@note'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'styled_tag')

	def test_determine_action_04(self):
		"""line is empty and self.spread = None """
		self.sut.spread = None
		line = ''
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'empty_line_detected')

	def test_determine_action_05(self):
		"""line is empty and self.spread is not a block tag """
		self.sut.spread = get_spread('@note')
		line = ''
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'empty_line_detected')

	def test_determine_action_06(self):
		"""line is empty and self.spread is a block tag """
		self.sut.spread = get_spread('@literal')
		line = ''
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'empty_line_detected')

	def test_determine_action_07(self):
		"""self.spread is a block tag and line is an end block tag """
		self.sut.spread = get_spread('@codeblock')
		line = '@endcodeblock'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'end_block_tag')

	def test_determine_action_08(self):
		"""self.spread is a block tag and line is not an end block tag """
		self.sut.spread = get_spread('@codeblock')
		line = '@table'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'end_block_tag')

	def test_determine_action_09(self):
		"""self.spread is a block tag """
		self.sut.spread = get_spread('@literal')
		line = 'anyvalue'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'start_block_tag')

	def test_determine_action_10(self):
		"""self.spread is a not block tag """
		self.sut.spread = get_spread('@table')
		line = 'anyvalue'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'start_block_tag')

	def test_determine_action_11(self):
		"""self.spread = None and line = * """
		self.sut.spread = None
		line = '* '
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'frontline_tag')

	def test_determine_action_12(self):
		"""self.spread = None and line = ** """
		self.sut.spread = None
		line = '** '
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'frontline_tag')

	def test_determine_action_13(self):
		"""self.spread is not an inline_allowed tag and line = * """
		self.sut.spread = get_spread('@note')
		line = '* '
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'frontline_tag')

	def test_determine_action_14(self):
		"""self.spread is not an inline_allowed tag and line = ** """
		self.sut.spread = get_spread('@note')
		line = '** '
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'frontline_tag')

	def test_determine_action_15(self):
		"""self.spread is not an inline_allowed tag and line = * """
		self.sut.spread = get_spread('@class')
		line = '* '
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'frontline_tag')

	def test_determine_action_16(self):
		"""self.spread is not an inline_allowed tag and line = ** """
		self.sut.spread = get_spread('@class')
		line = '** '
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'frontline_tag')

	def test_determine_action_17(self):
		"""self.spread = None and line is a header """
		self.sut.spread = None
		line = '@h2 Section Title'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'headers_and_markers')

	def test_determine_action_18(self):
		"""self.spread = None and line is a marker """
		self.sut.spread = None
		line = '@m2 Section Title'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'headers_and_markers')

	def test_determine_action_19(self):
		"""self.spread is not an inline_allowed tag and line is a header """
		self.sut.spread = get_spread('@note')
		line = '@h2 Section Title'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'headers_and_markers')

	def test_determine_action_20(self):
		"""self.spread is not an inline_allowed tag and line is a marker """
		self.sut.spread = get_spread('@note')
		line = '@m2 Section Title'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'headers_and_markers')

	def test_determine_action_21(self):
		"""self.spread is not an inline_allowed tag and line is a header """
		self.sut.spread = get_spread('@class')
		line = '@h2 Section Title'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'headers_and_markers')

	def test_determine_action_22(self):
		"""self.spread is not an inline_allowed tag and line is a marker """
		self.sut.spread = get_spread('@class')
		line = '@m2 Section Title'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'headers_and_markers')

	def test_determine_action_23(self):
		"""line is 5 underscores (_) """
		line = '_____'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'horizontal_rule')

	def test_determine_action_24(self):
		"""line is 1 underscore (_) """
		line = '_'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'horizontal_rule')

	def test_determine_action_25(self):
		"""line is 2 underscores and a minus (__-) """
		line = '__-'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'horizontal_rule')

	def test_determine_action_26(self):
		"""self.spread is not None """
		self.sut.spread = get_spread('@note')
		line = 'This is a note.'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'add_to_existing_spread')

	def test_determine_action_27(self):
		"""self.spread is None """
		self.sut.spread = None
		line = 'This is a note.'
		result = self.sut.determine_action(line)

		self.assertNotEqual(result, 'add_to_existing_spread')

	def test_determine_action_28(self):
		"""self.spread = None and prev_line is not an output control """
		self.sut.spread = None
		self.sut.prev_line = 'This was the previous line.'
		line = 'This is a paragraph.'
		result = self.sut.determine_action(line)

		self.assertEqual(result, 'header_or_paragraph')

	def test_determine_action_29(self):
		"""self.spread = None and prev_line is an output control """
		self.sut.spread = None
		self.sut.prev_line = '@page'
		line = 'This is a line of text.'
		result = self.sut.determine_action(line)

		self.assertEqual(result, None)

	#--------------------------------------------------
	# TEST CASES - add_spread()
	#--------------------------------------------------

	def test_add_spread_1(self):
		"""self.spread = None """
		self.sut.document = []
		self.sut.spread = None
		self.sut.prev_spread = None
		self.sut.empty_lines = 2
		self.sut.add_spread()

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.spread, None)
		self.assertEqual(self.sut.prev_spread, None)
		self.assertEqual(self.sut.empty_lines, 0)

	def test_add_spread_2(self):
		"""self.spread = None and preserve_empty = True """
		self.sut.document = []
		self.sut.spread = None
		self.sut.prev_spread = None
		self.sut.empty_lines = 2
		self.sut.add_spread(preserve_empty = True)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.spread, None)
		self.assertEqual(self.sut.prev_spread, None)
		self.assertEqual(self.sut.empty_lines, 2)

	def test_add_spread_3(self):
		"""self.spread is not None and is empty """
		self.sut.document = []
		self.sut.spread = get_spread('@note')
		self.sut.prev_spread = None
		self.sut.empty_lines = 2
		self.sut.add_spread()

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.spread.type, '@note')
		self.assertEqual(self.sut.prev_spread, None)
		self.assertEqual(self.sut.empty_lines, 0)

	def test_add_spread_4(self):
		"""self.spread is not None and is not empty """
		self.sut.document = []
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('Text')
		self.sut.prev_spread = None
		self.sut.empty_lines = 2
		self.sut.add_spread()

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.spread, None)
		self.assertEqual(self.sut.prev_spread.type, '@note')
		self.assertEqual(self.sut.empty_lines, 0)

	def test_add_spread_5(self):
		"""self.spread is not None and end_spread = False"""
		self.sut.document = []
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('Text')
		self.sut.prev_spread = None
		self.sut.empty_lines = 2
		self.sut.add_spread(end_spread = False)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.spread.type, '@note')
		self.assertEqual(self.sut.prev_spread.type, '@note')
		self.assertEqual(self.sut.empty_lines, 0)

	#--------------------------------------------------
	# TEST CASES - parse_text() and render()
	#--------------------------------------------------

	def test_parse_text_and_render_1(self):
		filepath = path('data', 'alltags.kp')
		fullpath = os.path.abspath(filepath)
		result = ''
		expected = ''
		with open(fullpath, 'r') as myfile:
			self.sut.parse_text(myfile)
		result = self.sut.render()
		with open(path('data', 'example-file-partial.html'), 'r') as outfile:
			expected = outfile.read()

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - styled_tag()
	#--------------------------------------------------

	def test_styled_tag_1(self):
		"""self.spread = None and line is an output control """
		self.sut.document = []
		self.sut.spread = None
		line = '@page'
		self.sut.styled_tag(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.spread, None)

	def test_styled_tag_2(self):
		"""self.spread = None and line is not an output control """
		self.sut.document = []
		self.sut.spread = None
		line = '@table'
		self.sut.styled_tag(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.spread.type, '@table')

	def test_styled_tag_3(self):
		"""self.spread is not None and line = @hr """
		self.sut.document = []
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '@hr'
		self.sut.styled_tag(line=line)

		self.assertEqual(len(self.sut.document), 2)
		self.assertEqual(self.sut.spread, None)

	#--------------------------------------------------
	# TEST CASES - simple tags
	#--------------------------------------------------

	def test_empty_line_detected(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		self.sut.empty_line_detected()

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 3)
		self.assertEqual(self.sut.spread, None)

	def test_start_block_tag(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@codeblock')
		line = '  This is code.'
		self.sut.start_block_tag(unstripped_line = line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@codeblock')
		self.assertEqual(self.sut.spread.text, '  This is code.')

	def test_end_block_tag(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@codeblock')
		self.sut.spread.add_text('  This is code.')
		self.sut.end_block_tag()

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread, None)

	def test_headers_and_markers(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '@h3 Header 3'
		self.sut.headers_and_markers(line=line)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@h3')
		self.assertEqual(self.sut.spread.text, ' Header 3')

	def test_horizontal_rule(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		self.sut.horizontal_rule()

		self.assertEqual(len(self.sut.document), 2)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread, None)

	def test_add_to_existing_spread(self):
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		line = 'This is text.'
		self.sut.add_to_existing_spread(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@note')
		self.assertEqual(self.sut.spread.text, ' This is text.')

	#--------------------------------------------------
	# TEST CASES - frontline_tag()
	#--------------------------------------------------

	def test_frontline_tag_1(self):
		"""non-bulleted list """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '-- my list item'
		self.sut.frontline_tag(line=line)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@non-bulleted')
		self.assertEqual(self.sut.spread.text, '\n-- my list item')

	def test_frontline_tag_2(self):
		"""bulleted list """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '*** my list item'
		self.sut.frontline_tag(line=line)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@bulleted')
		self.assertEqual(self.sut.spread.text, '\n*** my list item')

	def test_frontline_tag_3(self):
		"""numbered list """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '# my list item'
		self.sut.frontline_tag(line=line)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@numbered')
		self.assertEqual(self.sut.spread.text, '\n# my list item')

	def test_frontline_tag_4(self):
		"""invalid list marker """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = get_spread('@note')
		self.sut.spread.add_text('This is a note.')
		line = '+ my list item'
		self.sut.frontline_tag(line=line)

		self.assertEqual(len(self.sut.document), 1)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, None)
		self.assertEqual(self.sut.spread.text, ' + my list item')

	#--------------------------------------------------
	# TEST CASES - header_or_paragraph()
	#--------------------------------------------------

	def test_header_or_paragraph_1(self):
		"""3 empty lines """
		self.sut.document = []
		self.sut.empty_lines = 3
		self.sut.spread = None
		line = 'any text here'
		self.sut.header_or_paragraph(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@h1')
		self.assertEqual(self.sut.spread.text, ' any text here')

	def test_header_or_paragraph_2(self):
		"""2 empty lines """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.spread = None
		line = 'any text here'
		self.sut.header_or_paragraph(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, '@h2')
		self.assertEqual(self.sut.spread.text, ' any text here')

	def test_header_or_paragraph_3(self):
		"""3 empty lines and self.newline_headers = False """
		self.sut.document = []
		self.sut.empty_lines = 3
		self.sut.newline_headers = False
		self.sut.spread = None
		line = 'any text here'
		self.sut.header_or_paragraph(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, None)
		self.assertEqual(self.sut.spread.text, ' any text here')

	def test_header_or_paragraph_4(self):
		"""2 empty lines and self.newline_headers = False """
		self.sut.document = []
		self.sut.empty_lines = 2
		self.sut.newline_headers = False
		self.sut.spread = None
		line = 'any text here'
		self.sut.header_or_paragraph(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, None)
		self.assertEqual(self.sut.spread.text, ' any text here')

	def test_header_or_paragraph_5(self):
		"""1 empty line """
		self.sut.document = []
		self.sut.empty_lines = 1
		self.sut.spread = None
		line = 'any text here'
		self.sut.header_or_paragraph(line=line)

		self.assertEqual(len(self.sut.document), 0)
		self.assertEqual(self.sut.empty_lines, 0)
		self.assertEqual(self.sut.spread.type, None)
		self.assertEqual(self.sut.spread.text, ' any text here')
