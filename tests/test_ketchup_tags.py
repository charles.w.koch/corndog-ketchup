from .base import CandKBaseCase
from candk.ketchup.spread import RenderTypes
from candk.ketchup.tags import *

class TestKetchupTags(CandKBaseCase):

	def setUp(self):
		pass

	def tearDown(self):
		pass

	def prepare_sut(self, tag, text, render_type, preserve_linebreaks=False):
		sut = get_spread(tag)
		for line in text.split('\n'):
			if preserve_linebreaks:
				line = line + '\n'
			sut.add_text(line, render_type=render_type)
		return sut

	def assertHtml(self, tag, text, expected, preserve_linebreaks=False):
		index = 9
		render_type = RenderTypes.HTML
		sut = self.prepare_sut(tag, text, render_type, preserve_linebreaks=preserve_linebreaks)
		result = sut.render(str(index), render_type=render_type)
		self.assertEqual(result, expected)

	def assertMarkdown(self, tag, text, expected, preserve_linebreaks=False):
		index = 9
		render_type = RenderTypes.MARKDOWN
		sut = self.prepare_sut(tag, text, render_type, preserve_linebreaks=preserve_linebreaks)
		result = sut.render(str(index), render_type=render_type)
		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - Ketchup Tags
	#--------------------------------------------------

	def test_paragraph_tag(self):
		tag = None
		text = '''Text *with* some /inline styling!'''
		html_expected = '<div id="9" class="k-paragraph"> Text <strong>with</strong> some <span class="k-code">inline</span> styling!</div>'
		md_expected = '\n Text **with** some ```inline```styling!\n'
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_imports_tag(self):
		tag = '@imports'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-imports">
Imports:
<div class="k-imports-line">Text *with* some /inline styling!</div>
<div class="k-imports-line">and another one!</div>
<div class="k-imports-line">yet again</div>
</div>'''
		md_expected = '''**Imports:**

* ```Text *with* some /inline styling!```
* ```and another one!```
* ```yet again```

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)


	def test_byline_tag(self):
		tag = '@byline'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-byline">
<div class="k-byline-line">Text <strong>with</strong> some <span class="k-code">inline</span> styling!</div>
<div class="k-byline-line">and another one!</div>
<div class="k-byline-line">yet again</div>
</div>'''
		md_expected = '''> * Text **with** some ```inline```styling!
> * and another one!
> * yet again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_class_tag(self):
		tag = '@class'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-class"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''## ``` Text *with* some /inline styling! and another one! yet again```

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_method_tag(self):
		tag = '@method'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-method"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''#### ``` Text *with* some /inline styling! and another one! yet again```
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_h1_tag(self):
		tag = '@h1'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-h1"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''#  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_h2_tag(self):
		tag = '@h2'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-h2"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''##  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_h3_tag(self):
		tag = '@h3'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-h3"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''###  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_h4_tag(self):
		tag = '@h4'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-h4"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''####  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_m1_tag(self):
		tag = '@m1'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-m1"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''#  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_m2_tag(self):
		tag = '@m2'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-m2"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''##  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_m3_tag(self):
		tag = '@m3'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-m3"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''###  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_m4_tag(self):
		tag = '@m4'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div id="9" class="k-m4"> Text *with* some /inline styling! and another one! yet again</div>'''
		md_expected = '''####  Text *with* some /inline styling! and another one! yet again
'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_hr_tag(self):
		tag = '@hr'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<hr />\n'''
		md_expected = '''----------------------------------------\n'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_br_tag(self):
		tag = '@br'
		text = '''Text *with* some /inline styling!
and another one!
yet again'''
		html_expected = '''<div class="k-break"></div>\n'''
		md_expected = '''\n\n'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_codeblock_tag(self):
		tag = '@codeblock'
		text = '''Text *with* some /inline styling!
	and another one!
		yet again
'''
		html_expected = '''<div id="9" class="k-codeblock">Text *with* some /inline styling!
	and another one!
		yet again

</div>'''
		md_expected = '''```
Text *with* some /inline styling!
	and another one!
		yet again


```

'''
		self.assertHtml(tag, text, html_expected, preserve_linebreaks=True)
		self.assertMarkdown(tag, text, md_expected, preserve_linebreaks=True)

	def test_literal_tag(self):
		tag = '@literal'
		text = '''Text *with* some /inline styling!
	and <tags> </and> another one!
		yet again'''
		html_expected = '''Text *with* some /inline styling!<br/>
	and <tags> </and> another one!<br/>
		yet again<br/>
'''
		md_expected = '''Text *with* some /inline styling!
	and <tags> </and> another one!
		yet again


'''
		self.assertHtml(tag, text, html_expected, preserve_linebreaks=True)
		self.assertMarkdown(tag, text, md_expected, preserve_linebreaks=True)

	def test_note_tag(self):
		tag = '@note'
		text = '''Text *with* some /inline styling!
and <tags> another one!
yet again'''
		html_expected = '''<table id="9" class="k-note"><tr><td class="k-note-note">NOTE:</td><td> Text <strong>with</strong> some <span class="k-code">inline</span> styling! and &lt;tags&gt; another one! yet again</td></tr></table>'''
		md_expected = '''**NOTE:**  Text **with** some ```inline```styling! and <tags> another one! yet again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_return_tag(self):
		tag = '@return'
		text = '''Text *with* some /inline styling!
and <tags> another one!
yet again'''
		html_expected = '''<div id="9" class="k-return">Returns:<div class="k-return_desc"> Text <strong>with</strong> some <span class="k-code">inline</span> styling! and &lt;tags&gt; another one! yet again</div></div>'''
		md_expected = '''**Returns:**  Text **with** some ```inline```styling! and <tags> another one! yet again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_deflist_tag(self):
		tag = '@deflist'
		text = '''Text: *with* some
/inline styling!
and: <tags> another one!
yet: again'''
		html_expected = '''<div id="9" class="k-params"><table class="k-def">
<tr><td class="k-var_name">Text</td><td class="k-var_desc"><strong>with</strong> some <span class="k-code">inline</span> styling!</td></tr>
<tr><td class="k-var_name">and</td><td class="k-var_desc">&lt;tags&gt; another one!</td></tr>
<tr><td class="k-var_name">yet</td><td class="k-var_desc">again</td></tr>
</table></div>'''
		md_expected = '''* ```Text```: **with** some ```inline```styling!
* ```and```: <tags> another one!
* ```yet```: again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_params_tag(self):
		tag = '@params'
		text = '''Text: *with* some
/inline styling!
and: <tags> another one!
yet: again'''
		html_expected = '''<div id="9" class="k-params">Parameters:<table class="k-params">
<tr><td class="k-var_name">Text</td><td class="k-var_desc"><strong>with</strong> some <span class="k-code">inline</span> styling!</td></tr>
<tr><td class="k-var_name">and</td><td class="k-var_desc">&lt;tags&gt; another one!</td></tr>
<tr><td class="k-var_name">yet</td><td class="k-var_desc">again</td></tr>
</table></div>'''
		md_expected = '''**Parameters:**

* ```Text```: **with** some ```inline```styling!
* ```and```: <tags> another one!
* ```yet```: again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_attributes_tag(self):
		tag = '@attributes'
		text = '''Text: *with* some
/inline styling!
and: <tags> another one!
yet: again'''
		html_expected = '''<div id="9" class="k-params">Attributes:<table class="k-attributes">
<tr><td class="k-var_name">Text</td><td class="k-var_desc"><strong>with</strong> some <span class="k-code">inline</span> styling!</td></tr>
<tr><td class="k-var_name">and</td><td class="k-var_desc">&lt;tags&gt; another one!</td></tr>
<tr><td class="k-var_name">yet</td><td class="k-var_desc">again</td></tr>
</table></div>'''
		md_expected = '''**Attributes:**

* ```Text```: **with** some ```inline```styling!
* ```and```: <tags> another one!
* ```yet```: again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_exceptions_tag(self):
		tag = '@exceptions'
		text = '''Text: *with* some
/inline styling!
and: <tags> another one!
yet: again'''
		html_expected = '''<div id="9" class="k-params">Exceptions:<table class="k-exceptions">
<tr><td class="k-var_name">Text</td><td class="k-var_desc"><strong>with</strong> some <span class="k-code">inline</span> styling!</td></tr>
<tr><td class="k-var_name">and</td><td class="k-var_desc">&lt;tags&gt; another one!</td></tr>
<tr><td class="k-var_name">yet</td><td class="k-var_desc">again</td></tr>
</table></div>'''
		md_expected = '''**Exceptions:**

* ```Text```: **with** some ```inline```styling!
* ```and```: <tags> another one!
* ```yet```: again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_constants_tag(self):
		tag = '@constants'
		text = '''Text: *with* some
/inline styling!
and: <tags> another one!
yet: again'''
		html_expected = '''<div id="9" class="k-params">Constants:<table class="k-constants">
<tr><td class="k-var_name">Text</td><td class="k-var_desc"><strong>with</strong> some <span class="k-code">inline</span> styling!</td></tr>
<tr><td class="k-var_name">and</td><td class="k-var_desc">&lt;tags&gt; another one!</td></tr>
<tr><td class="k-var_name">yet</td><td class="k-var_desc">again</td></tr>
</table></div>'''
		md_expected = '''**Constants:**

* ```Text```: **with** some ```inline```styling!
* ```and```: <tags> another one!
* ```yet```: again

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_nonbulleted_list_tag(self):
		tag = '@non-bulleted'
		text = '''- Text *with* some /inline styling!
-- and <tags> another one!
--- yet again
- back to level 1'''
		html_expected = '''<ul id="9" class="k-non-bulleted">
<li>Text <strong>with</strong> some <span class="k-code">inline</span> styling!</li>
<ul>
<li>and &lt;tags&gt; another one!</li>
<ul>
<li>yet again</li>
</ul>
</ul>
<li>back to level 1</li>
</ul>
'''
		md_expected = '''- Text **with** some ```inline```styling!
  - and <tags> another one!
    - yet again
- back to level 1

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_bulleted_list_tag(self):
		tag = '@bulleted'
		text = '''* Text *with* some /inline styling!
** and <tags> another one!
*** yet again
* back to level 1'''
		html_expected = '''<ul id="9" class="k-bulleted">
<li>Text <strong>with</strong> some <span class="k-code">inline</span> styling!</li>
<ul>
<li>and &lt;tags&gt; another one!</li>
<ul>
<li>yet again</li>
</ul>
</ul>
<li>back to level 1</li>
</ul>
'''
		md_expected = '''* Text **with** some ```inline```styling!
  * and <tags> another one!
    * yet again
* back to level 1

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_numbered_list_tag(self):
		tag = '@numbered'
		text = '''# Text *with* some /inline styling!
## and <tags> another one!
### yet again
# back to level 1'''
		html_expected = '''<ol id="9" class="k-numbered">
<li>Text <strong>with</strong> some <span class="k-code">inline</span> styling!</li>
<ol>
<li>and &lt;tags&gt; another one!</li>
<ol>
<li>yet again</li>
</ol>
</ol>
<li>back to level 1</li>
</ol>
'''
		md_expected = '''1 Text **with** some ```inline```styling!
  1 and <tags> another one!
    1 yet again
1 back to level 1

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_table_tag(self):
		tag = '@table'
		text = '''name    | required? | description
--------------------------------------------------
name1 | yes | this is description 1
name2 is | | this is description 2
Name3 | maybe | this is description 3'''
		html_expected = '''<table class="k-table">
<tr><th>name</th><th>required?</th><th>description</th></tr>
<tr><td>name1</td><td>yes</td><td>this is description 1</td></tr>
<tr><td>name2 is</td><td></td><td>this is description 2</td></tr>
<tr><td>Name3</td><td>maybe</td><td>this is description 3</td></tr>
</table>
'''
		md_expected = '''| name | required? | description |
|---|---|---|
| name1 | yes | this is description 1 |
| name2 is |  | this is description 2 |
| Name3 | maybe | this is description 3 |

'''
		self.assertHtml(tag, text, html_expected)
		self.assertMarkdown(tag, text, md_expected)

	def test_image_tag(self):
			tag = '@image'
			text = '''http://myimage.png height=400px
	width=100em'''
			html_expected = '''<img class="k-image" src="http://myimage.png" style="height:400px;width:100em;" />'''
			md_expected = '''![](http://myimage.png)
'''
			self.assertHtml(tag, text, html_expected)
			self.assertMarkdown(tag, text, md_expected)