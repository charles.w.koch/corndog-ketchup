
//This is a comment
//-== with corndog parsing part of it.

//@class
public class MyClass {
	/*-==This is the class docstring. */



	//-==@method
	def main(self):
		/**-==This is a multi-line docstring.
* Whoops the indentation is off.
* That shouldn't matter.*/

		String this = "should not be captured.";
		String neither = "should this.";
	}

	/*-==@h1 There's stuff here.
	* And then the next line.
	* -==This adds a newline in between.
	*
	* This should not be captured.
	*/
	public void method_not_captured() {
		// This should not be captured.
	}
