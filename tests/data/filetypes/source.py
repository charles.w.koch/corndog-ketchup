
#This is a comment
#-== with corndog parsing part of it.

#@class
class MyClass:
	'''-==This is the class docstring. '''



	#-==@method
	def main(self):
		"""-==This is a multi-line docstring.
Whoops the indentation is off.
That shouldn't matter."""

		this = 'should not be captured.'
		neither = 'should this.'

	#-==@h1 There's stuff here.
	# And then the next line.
	#-==This adds a newline in between.
	#
	# This should not be captured.

	def method_not_captured():
		# This should not be captured.
		pass
		