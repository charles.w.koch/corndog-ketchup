
::This is a comment
::-== with corndog parsing part of it.

REM @class
class MyClass:
	-==This is the class docstring. 



	REM -==@method
	def main(self):
		rem -==This is a multi-line docstring.
rem Whoops the indentation is off.
rem That shouldn't matter.

		this = 'should not be captured.'
		neither = 'should this.'

	::-==@h1 There's stuff here.
	:: And then the next line.
	::-==This adds a newline in between.
	::
	:: This should not be captured.

	def method_not_captured():
		rem This should not be captured.
		pass
		