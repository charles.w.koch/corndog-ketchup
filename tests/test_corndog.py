import contextlib
import io, os
import shutil
import unittest
from .base import CandKBaseCase, path, TEST_ROOT, OUTPUT_DIR

from candk.corndog import Corndog, STREAM, SINGLE_FILE, ONE_TO_ONE


def generic_expected(filename):
	return '''
 {}


 This has text in it!
And it will be captured.


 This captured.
'''.format(filename)


class TestCorndogBasic(CandKBaseCase):

	def setUp(self):
		self.sut = Corndog(
			start_point = path('data', 'subdir1'),
			search_subs = True,
			output = SINGLE_FILE,
			output_dir = OUTPUT_DIR)

	def tearDown(self):
		pass

	#--------------------------------------------------
	# TEST CASES - initialize_data()
	#--------------------------------------------------

	def test_initialize_data_1(self):
		"""All blank (default) data"""
		os.chdir(TEST_ROOT)
		self.sut = Corndog()
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, TEST_ROOT)
		self.assertEqual(self.sut.original_start, TEST_ROOT)
		self.assertEqual(self.sut.output_dir, TEST_ROOT)
		self.assertEqual(len(self.sut.excludes), 0)
		self.assertEqual(len(self.sut._exclude_paths), 0)

	def test_initialize_data_2(self):
		"""With start_point set to a directory"""
		start = 'data'
		absstart = path('data')
		self.sut = Corndog(start_point=start)
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, absstart)
		self.assertEqual(self.sut.original_start, absstart)
		self.assertEqual(self.sut.output_dir, absstart)
		self.assertEqual(len(self.sut.excludes), 0)
		self.assertEqual(len(self.sut._exclude_paths), 0)

	def test_initialize_data_3(self):
		"""With output_dir set to a directory"""
		outdir = 'output'
		absoutdir = OUTPUT_DIR
		self.sut = Corndog(output_dir=outdir)
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, TEST_ROOT)
		self.assertEqual(self.sut.original_start, TEST_ROOT)
		self.assertEqual(self.sut.output_dir, absoutdir)
		self.assertEqual(len(self.sut.excludes), 0)
		self.assertEqual(len(self.sut._exclude_paths), 0)

	def test_initialize_data_4(self):
		"""With start_point set to a file"""
		start = 'data/file1.txt'
		absstart = path('data')
		absstartfile = path('data', 'file1.txt')
		self.sut = Corndog(start_point=start)
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, absstart)
		self.assertEqual(self.sut.original_start, absstartfile)
		self.assertEqual(self.sut.output_dir, absstart)
		self.assertEqual(len(self.sut.excludes), 0)
		self.assertEqual(len(self.sut._exclude_paths), 0)

	def test_initialize_data_5(self):
		"""With start_point set to a file, and output_dir set to a directory"""
		start = 'data/file1.txt'
		absstart = path('data')
		absstartfile = path('data', 'file1.txt')
		outdir = 'output'
		absoutdir = OUTPUT_DIR
		self.sut = Corndog(start_point=start, output_dir=outdir)
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, absstart)
		self.assertEqual(self.sut.original_start, absstartfile)
		self.assertEqual(self.sut.output_dir, absoutdir)
		self.assertEqual(len(self.sut.excludes), 0)
		self.assertEqual(len(self.sut._exclude_paths), 0)

	def test_initialize_data_6(self):
		"""With excludes provided"""
		os.chdir(TEST_ROOT)
		excludes = ['**/alltags.kp', '**/*.bat', '*corndog*']
		self.sut = Corndog(excludes=excludes)
		self.sut.initialize_data()

		self.assertEqual(self.sut.start_point, TEST_ROOT)
		self.assertEqual(self.sut.original_start, TEST_ROOT)
		self.assertEqual(self.sut.output_dir, TEST_ROOT)
		self.assertEqual(len(self.sut.excludes), 3)
		self.assertEqual(len(self.sut._exclude_paths), 4)
		self.assertEqual(self.sut._exclude_paths[0], path('data', 'alltags.kp'))
		self.assertEqual(self.sut._exclude_paths[1], path('data', 'filetypes', 'source.bat'))
		self.assertEqual(self.sut._exclude_paths[2], path('test_corndog_sources.py'))
		self.assertEqual(self.sut._exclude_paths[3], path('test_corndog.py'))

	#--------------------------------------------------
	# TEST CASES - send_to_output()
	#--------------------------------------------------

	def test_send_to_output_1(self):
		"""Output to SINGLE_FILE with no output_dir"""
		filepath = path('Corndog_out.txt')
		expected = 'This is the text.'
		os.chdir(TEST_ROOT)
		self.sut.send_to_output('This is the text.', SINGLE_FILE)
		
		# self.assertTrue(os.path.exists(path('Corndog_out.txt')))
		# result = ''
		# with open(path('Corndog_out.txt'), 'r') as outfile:
		# 	result = outfile.read()
		# self.assertEqual(result, 'This is the text.')
		self.assertFileContents(filepath, expected)

	def test_send_to_output_2(self):
		"""Output to SINGLE_FILE"""
		filepath = path('output', 'send_to_output_2.html')
		expected = 'This is the text.'
		self.sut.send_to_output('This is the text.', SINGLE_FILE,
								output_name='send_to_output_2',
								output_ext='.html',
								output_dir=OUTPUT_DIR)

		self.assertFileContents(filepath, expected)

	def test_send_to_output_3(self):
		"""Output to ONE_TO_ONE"""
		filepath = path('output', 'send_to_output_3.txt')
		expected = 'This is the text.'
		self.sut.send_to_output('This is the text.', SINGLE_FILE,
								output_name='send_to_output_3',
								output_dir=OUTPUT_DIR)

		self.assertFileContents(filepath, expected)

	def test_send_to_output_4(self):
		"""Output to STREAM"""
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.send_to_output('This is the text.', STREAM,
								output_name='send_to_output_4',
								output_dir=OUTPUT_DIR)
			result = outstream.getvalue()

		self.assertFalse(os.path.exists(path('output', 'send_to_output_4.txt')))
		self.assertEqual(result, 'This is the text.\n')

	#--------------------------------------------------
	# TEST CASES - capture_text()
	#--------------------------------------------------

	def test_capture_text_1(self):
		"""Blank line """
		line = ''
		file_ext = '.py'
		result = self.sut.capture_text(line, file_ext)
		self.assertEqual(result, '\n')

	def test_capture_text_2(self):
		"""Blank comment line """
		line = '# -==   '
		file_ext = '.py'
		result = self.sut.capture_text(line, file_ext)
		self.assertEqual(result, '\n')

	def test_capture_text_3(self):
		"""Comment line with corndog token """
		line = '# -==    This is a comment. '
		file_ext = '.py'
		result = self.sut.capture_text(line, file_ext)
		self.assertEqual(result, '\n    This is a comment. ')

	def test_capture_text_4(self):
		"""Comment line, no corndog token """
		line = '#     This is a comment. '
		file_ext = '.py'
		result = self.sut.capture_text(line, file_ext)
		self.assertEqual(result, 'This is a comment. ')

	#--------------------------------------------------
	# TEST CASES - parse_file()
	#--------------------------------------------------

	def test_parse_file_1(self):
		"""Valid file with corndog token """
		filepath = path('data', 'file1.txt')
		expected = generic_expected('File 1')
		result = self.sut.parse_file(filepath)

		self.assertEqual(result, expected)

	def test_parse_file_2(self):
		"""Valid file with no corndog tokens """
		filepath = path('data', 'subdir1', 'file-no-corndog.txt')
		expected = ''
		result = self.sut.parse_file(filepath)

		self.assertEqual(result, expected)

	def test_parse_file_3(self):
		"""Valid file with corndog token, but it's empty """
		filepath = path('data', 'file2.txt')
		expected = '\n'
		result = self.sut.parse_file(filepath)

		self.assertEqual(result, expected)

	def test_parse_file_4(self):
		"""Not valid file with corndog tokens """
		filepath = path('data', 'file3.uml')
		expected = ''
		result = self.sut.parse_file(filepath)

		self.assertEqual(result, expected)

	#--------------------------------------------------
	# TEST CASES - begin()
	#--------------------------------------------------

	def fully_recursive_output(self):
		expected = ''
		expected += generic_expected('File 1 - 1') + '\n\n\n'
		expected += generic_expected('File 1 - 1 - 1 - 1') + '\n'
		expected += generic_expected('File 1 - 1 - 1 - 2') + '\n'
		expected += generic_expected('File 1 - 2 - 1') + '\n'
		expected += generic_expected('File 1 - 2 - 2') + '\n'
		return expected

	def test_begin_1(self):
		"""directory with SINGLE_FILE """
		self.sut.output_name = 'begin_test_1'
		self.sut.output = SINGLE_FILE
		expected = self.fully_recursive_output()
		filepath = path('output','{}.txt'.format(self.sut.output_name))
		
		self.sut.begin()
		self.assertFileContents(filepath, expected)

	def test_begin_2(self):
		"""directory with ONE_TO_ONE """
		self.sut.output_name = 'begin_test_2'
		self.sut.output = ONE_TO_ONE
		self.sut.begin()

		filelist = [
			{'name': 'file1-1', 'title': 'File 1 - 1'},
			{'name': 'subdir1-1-subdir1-1-1-file1-1-1-1', 'title': 'File 1 - 1 - 1 - 1'},
			{'name': 'subdir1-1-subdir1-1-1-file1-1-1-2', 'title': 'File 1 - 1 - 1 - 2'},
			{'name': 'subdir1-2-file1-2-1', 'title': 'File 1 - 2 - 1'},
			{'name': 'subdir1-2-file1-2-2', 'title': 'File 1 - 2 - 2'},
		]
		for file in filelist:
			expected = generic_expected(file['title'])
			filepath = path('output','{}.txt'.format(file['name']))
			self.assertFileContents(filepath, expected)

	def test_begin_3(self):
		"""directory with STREAM """
		self.sut.output_name = 'begin_test_3'
		self.sut.output = STREAM
		expected = self.fully_recursive_output()
		filepath = path('output','{}.txt'.format(self.sut.output_name))
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertFalse(os.path.exists(filepath))
		self.assertEqual(result, expected)

	def test_begin_4(self):
		"""directory with search_subs = False """
		self.sut.output_name = 'begin_test_3'
		self.sut.output = STREAM
		self.sut.search_subs = False
		expected = generic_expected('File 1 - 1') + '\n\n\n'
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_begin_5(self):
		"""file with SINGLE_FILE """
		self.sut.output_name = 'begin_test_5'
		self.sut.start_point = path('data', 'file1.txt')
		self.sut.output = SINGLE_FILE
		expected = generic_expected('File 1') 
		filepath = path('output','{}.txt'.format(self.sut.output_name))
		
		self.sut.begin()
		self.assertFileContents(filepath, expected)

	def test_begin_6(self):
		"""file with ONE_TO_ONE """
		self.sut.output_name = 'begin_test_6'
		self.sut.start_point = path('data', 'file1.txt')
		self.sut.output = ONE_TO_ONE
		expected = generic_expected('File 1')
		filepath = path('output','{}.txt'.format(self.sut.output_name))
		
		self.sut.begin()
		self.assertFileContents(filepath, expected)

	def test_begin_7(self):
		"""file with STREAM """
		self.sut.output_name = 'begin_test_7'
		self.sut.start_point = path('data', 'file1.txt')
		self.sut.output = STREAM
		expected = generic_expected('File 1') + '\n'
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)

	def test_begin_8(self):
		"""invalid file """
		self.sut.output_name = 'begin_test_8'
		self.sut.start_point = path('data', 'file3.uml')
		self.sut.output = STREAM
		expected = '\n'
		result = ''
		with contextlib.redirect_stdout(io.StringIO()) as outstream:
			self.sut.begin()
			result = outstream.getvalue()

		self.assertEqual(result, expected)