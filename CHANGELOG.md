
# Changelog

All notable changes to this project will be documented in this file.


# 2.6.0

# New Features

- **C+K:** Added `-X` option to exclude files based on a file glob pattern
- **Corndog:** Added `-X` option to exclude files based on a file glob pattern
  - This can be used multiple times to exclude multiple patterns
- **C+K:** Verbosity level can be set with multiple v's (`-v`, `-vv`, or `-vvv`)

# 2.5.0

## New Features

- **C+K:** Added `-v` option for verbose output
- **Corndog:** added support for Vue.js files (`.vue`)
- **Corndog:** added support for TypeScript files (`.ts`)
- **Corndog:** added support for TOML files (`.toml`)
- **Corndog:** added support for YAML files (`.yml` / `.yaml`)
- **Corndog:** added support for INI files (`.ini`)
- **Corndog:** added support for Conf files (`.conf`)

## Deprecations

- **Corndog:** Support for TaskBuilder files (`.tas`) will be removed in a future version


# 2.4.0

## New Features

- **Ketchup:** Added new `@image` tag
- **Ketchup:** Added option `-b` to disable empty newlines marking text as a header
- **Ketchup:** Added `-M` to have same functionality as `-m` for parity with `candk` command
- **C+K:** Added `-p` option to change the title of page(s) generated (same as Ketchup's `-p`)
- **C+K:** Added `-b` option to disable empty newlines marking text as a header


# 2.3.0

## New Features

- **C+K:** Added `-c` option for embedding a stylesheet
- **C+K:** Added `-M` option for rendering in Markdown rather than HTML
- **C+K:** Added `-N` option for collapsible navigation
- **Ketchup:** Added collapsible navigation option
  - `@h1` headers can collapse to hide everything of a lower heading beneath it
  - This is useful for large documents where the navigation is very long
  - Use `-N` to enable collapsible navigation

## Deprecations

- **Ketchup:** The style of `@h1` navigation changed slightly to accomodate collapsible navigation

## Fixes and Maintenance

- **Ketchup:** Improved the CSS minifying that takes place before the stylesheet is embedded
- **Corndog:** Fixed an issue where files were being processed out of order
  - Files are now processed alphabetically, in a depth-first search algorithm
- **Corndog:** Fixed a bug in multi-file parsing where files were named by their absolute path rather than the relative path
- **Ketchup:** Fixed a bug with `@literalend` tag not working
- **Ketchup:** Fixed a bug with `@exceptions` tag not working


# 2.2.1

## Fixes and Maintenance

- Fixed issues with PyPI package publish and installation


# 2.2.0

## New Features

- Published to PyPI as `candk 2.2.0`


# 2.1.1

## Fixes and Maintenance

- **Ketchup:** added missing file which allow ketchup to be called from outside it's module


# 2.1.0

## New Features

- **Ketchup:** can now export to Markdown (`.md) files
  - `-m` exports document as a Markdown rather than HTML
- **Ketchup:** new return tag for convenience
  - `@returns` can be used instead of `@return`


# 2.0.0

## New Features

- **Ketchup:** new ending tags for block-level tags
  - `@codeblockend` can be used instead of `@endcodeblock`
  - `@literalend` can be used instead of `@endliteral`
- **Ketchup:** Headers 1, 2, and 3 can be parsed without `@h` tags by placing empty newlines above them
  - `@h1` requires 3 blank newlines
  - `@h2` requires 2 blank newlines
  - _**Note:** when headers are set like this, there must be an empty newline or another tag to indicate the end of the header_

## Breaking Changes

- **Ketchup:** since newlines now can denote headers, some documents may render strangely if they have multiple consecutive empty newlines in them

# 1.5.2

## Fixes and Maintenance

- **Ketchup:** added missing helpsections to help text
- **Ketchup:** fixed options with arguments crashing Ketchup


# 1.5.1

## Fixes and Maintenance

- **Ketchup:** fixed import errors which caused Ketchup to crash


# 1.5.0

## New Features

- **Ketchup:** inline hyperlinks can be included
  - `!` before a URL will turn it into a hyperlink
- **Ketchup:** custom CSS for documents
  - `-c <CSS filename>` command line option
- **Ketchup:** "watch" flag, automatically re-renders a file when changes are made
  - `-w` flag will set a wait loop where Ketchup will re-render the documentation whenever the source file changes


# 1.4.1

## Fixes and Maintenance

- **Ketchup:** fixed an issue where `@deflist` was not concatenating multiple lines


# 1.4.0

## New Features

- **Ketchup:** documents now include footer text which indicates the version of Ketchup used to render them
- **Ketchup:** new tag `@deflist` for definition lists
  - Has same behavior and styling as `@params`, `@exceptions`, `@attributes`, etc. but does not include a title
  - Useful for small tables of definitions that don't warrant an entire `@table` block


# 1.3.0

## New Features

- **Ketchup:** new, cleaner and easier to read document styling

## Fixes and Maintenance

- **Corndog:** updated command line options parsing to be more Unix-like
- **Ketchup:** updated command line options parsing to be more Unix-like
- **Ketchup:** fixed a bug in render style of class and method headers
- **Ketchup:** fixed issues with HTML characters being inserted directly into the document
- **Ketchup:** fixed an issue with inline line break (`@`) not being parsed
- **Ketchup:** fixed issues with `@codeblock` and `@literal` tags not rendering correctly


# 1.2.0

## New Features

- **Corndog:** added support for TaskBuilder files (.tas`)
  - Only description nodes (`:CD`) and comment nodes (`:CM`) will be considered for Corndog parsing

## Fixes and Maintenance

- **Corndog:** Python, Shell, and Properties comments no longer contain the leading #
  - Use a backslash (`\`) before a hash mark if you wish to use it at the beginning of a line
- **Corndog:** fixed an issue where a comment token with nothing after it was considered a valid parse, rather than an empty newline
- **Corndog:** fixed and issue where a new parsing section would not be started when encountering a corndog (`-==`) within a parsing section
- **Ketchup:** fixed an issue where Ketchup would sometimes insert an unwanted byline at the end of the output file
- **CandK:** fixed an issue where Ketchup files (`.kp`) would be left behind after parsing and transforming them


# 1.1.0

## New Features

- **Integration:** a new script (`candk.py`) combines Corndog parsing with Ketchup markup to allow processing source code files and generating documentation from them with a single command
  - `python3 candk.py -frmh -s [path or file] -o [path] -n [name] -x [ext]`
  - Use the help option (`-h`) to get info on specific options of this script
- **Corndog:** adds Ketchup markup files (`.kp`) to the available supported file extensions


# 1.0.0

## New Features

- **Ketchup:** can now be invoked as a command line script with proper CLI options
  - `python3 ketchup.py -h -p [page name] -n [output name] -o [output dir]`
  - Use the help option (`-h`) to get info on specific options of this script
- **Ketchup:** added new markup for non-code sections
  - `@h1`-`@h6` for headings which will be placed in the navigation sidebar
  - `@m1`-`@m6` for headings which are **not** placed in the navigation sidebar
  - `@table` for inserting a table of data
  - `@hr` for a horizontal rule
  - `@br` for a line break
  - `@codeblock` and `@endcodeblock` for inserting code
  - `@literal` and `@endliteral` for inserting raw text or HTML directly
  - `@note` to add a note in a special, eye-catching box
- **Corndog:** changed the way parsing happens
  - `CORNDOG FETCH` and `GOOD BOY CORNDOG` are no longer used
  - Instead, a corndog (`-==`) in the text will begin the parsing
  - An empty newline or the end of the file will end the parsing
  - If another corndog (`-==`) is encountered during parsing, it considers that to be a separate parse.
    - Current parse section will end and a new parsing section will begin

## Breaking Changes

- **Corndog:** changed the way parsing happens
  - `CORNDOG FETCH` and `GOOD BOY CORNDOG` are no longer used
  - See "New Features" above for usage of Corndog


# 0.0.0

Introducing Corndog + Ketchup! A pair of parsing and markup tools for quick and easy documentation generation.

## New Features

- **Corndog:** a simple script that parses out text when the magic phrase is encountered
  - CORNDOG FETCH marks the beginning of a section that Corndog will parse from a file
  - GOOD BOY CORNDOG marks the end of a section Corndog should be parsing
  - Automatically ignores comment tokens in supported file extensions:
    - Python (.py)
    - Java (.java)
    - C/C++ (.c, .cpp)
    - SQL (.sql)
    - XML/HTML (.xml, .html)
    - PHP (.php)
    - FreeMarker (.ftl)
    - JavaScript (.js)
    - CSS (.css)
    - Shell scripts (.sh)
    - Batch files (.bat)
    - Text files (.txt)
    - Properties files (.properties)
- **Ketchup:** Markdown-inspired code documentation markup
  - Provides special tokens to identify pieces of source files as document sections
    - `@byline` for authors and ownership comments
    - `@imports` for imported modules/dependencies
    - `@class` for class definitions
    - `@method` for function/method declarations
    - `@attributes` for attribute/property declarations
    - `@constants` for constants declarations
    - `@params` for object/function parameters
    - `@return` for return value description
    - `@exceptions` for possible exceptions thrown
